FROM python:3.9

RUN pip3 install requests && \
    mkdir -p /usr/CAM/app && \
    mkdir -p /usr/CAM/lib/digitalocean

COPY digitalocean/ /usr/CAM/lib/digitalocean

ENV PYTHONPATH /usr/CAM/lib
