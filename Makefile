PROGRAM = py-digitalocean
LABEL   = $(shell git rev-parse --abbrev-ref HEAD)

.PHONY: build unit acceptance

all: build unit

build: Dockerfile tests/Dockerfile
	docker build -t $(PROGRAM):$(LABEL) .
	mkdir tmp
	sed 's/__SRC_IMG__/$(PROGRAM):$(LABEL)/' tests/Dockerfile > tmp/Dockerfile
	docker build -t $(PROGRAM)-tests:$(LABEL) -f tmp/Dockerfile .
	rm -rf tmp

unit:
	docker run -t --rm $(PROGRAM)-tests:$(LABEL) /usr/CAM/tests/unit/exceptions_test.py
	docker run -t --rm $(PROGRAM)-tests:$(LABEL) /usr/CAM/tests/unit/region_test.py
	docker run -t --rm $(PROGRAM)-tests:$(LABEL) /usr/CAM/tests/unit/image_test.py
	docker run -t --rm $(PROGRAM)-tests:$(LABEL) /usr/CAM/tests/unit/size_test.py
	docker run -t --rm $(PROGRAM)-tests:$(LABEL) /usr/CAM/tests/unit/networks/v4_test.py
	docker run -t --rm $(PROGRAM)-tests:$(LABEL) /usr/CAM/tests/unit/networks/v6_test.py
	docker run -t --rm $(PROGRAM)-tests:$(LABEL) /usr/CAM/tests/unit/networks_test.py
	docker run -t --rm $(PROGRAM)-tests:$(LABEL) /usr/CAM/tests/unit/networks_test.py
	docker run -t --rm $(PROGRAM)-tests:$(LABEL) /usr/CAM/tests/unit/droplet_test.py
	docker run -t --rm $(PROGRAM)-tests:$(LABEL) /usr/CAM/tests/unit/window_test.py

acceptance:
	docker run -t --env-file env.txt $(PROGRAM)-tests:$(LABEL) /usr/CAM/tests/acceptance/region_test.py
	docker run -t --env-file env.txt $(PROGRAM)-tests:$(LABEL) /usr/CAM/tests/acceptance/size_test.py
	docker run -t --env-file env.txt $(PROGRAM)-tests:$(LABEL) /usr/CAM/tests/acceptance/droplet_test.py
