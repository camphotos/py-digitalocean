#!/usr/local/bin/python3

import unittest

from digitalocean.exceptions import *
from digitalocean.region import Region

class TestRegion(unittest.TestCase):

    def test_valid(self):
        #
        # Valid Args
        args = {
            'name' : "New York 3",
            'slug' : "nyc3",
            'features': [
                "private_networking",
                "backups",
                "ipv6",
                "metadata",
                "install_agent",
                "storage",
                "image_transfer"   
            ],
            'available': True,
            'sizes': [
                "s-1vcpu-1gb",
                "s-1vcpu-2gb",
                "s-1vcpu-3gb",
            ]
        }

        region = Region(**args)
        self.assertTrue(isinstance(region, Region))

        self.assertEqual(region.name(), "New York 3")
        self.assertEqual(region.slug(), "nyc3")
        self.assertEqual(len(region.features()), 7)
        self.assertEqual(region.features()[0], "private_networking")
        self.assertEqual(region.available(), True)
        self.assertEqual(len(region.sizes()), 3)
        self.assertEqual(region.sizes()[0], "s-1vcpu-1gb")

    def test_invalid_name(self):

        args = {
            'name' : 123,
            'slug' : "nyc3",
            'features': [
                "private_networking",
                "backups",
                "ipv6",
                "metadata",
                "install_agent",
                "storage",
                "image_transfer"   
            ],
            'available': True,
            'sizes': [
                "s-1vcpu-1gb",
                "s-1vcpu-2gb",
                "s-1vcpu-3gb",
            ]
        }

        with self.assertRaises(ValueError):
            region = Region(**args)

    def test_invalid_slug(self):
        args = {
            'name' : "New York 3",
            'slug' : 123,
            'features': [
                "private_networking",
                "backups",
                "ipv6",
                "metadata",
                "install_agent",
                "storage",
                "image_transfer"   
            ],
            'available': True,
            'sizes': [
                "s-1vcpu-1gb",
                "s-1vcpu-2gb",
                "s-1vcpu-3gb",
            ]
        }

        with self.assertRaises(ValueError):
            region = Region(**args)

    def test_invalid_features(self):
        args = {
            'name' : "New York 3",
            'slug' : "nyc3",
            'features': [
                "private_networking",
                "backups",
                123,
                "metadata",
                "install_agent",
                "storage",
                "image_transfer"   
            ],
            'available': True,
            'sizes': [
                "s-1vcpu-1gb",
                "s-1vcpu-2gb",
                "s-1vcpu-3gb",
            ]
        }

        with self.assertRaises(ValueError):
            region = Region(**args)

    def test_invalid_available(self):
        args = {
            'name' : "New York 3",
            'slug' : "nyc3",
            'features': [
                "private_networking",
                "backups",
                "ipv6",
                "metadata",
                "install_agent",
                "storage",
                "image_transfer"   
            ],
            'available': "invalid",
            'sizes': [
                "s-1vcpu-1gb",
                "s-1vcpu-2gb",
                "s-1vcpu-3gb",
            ]
        }

        with self.assertRaises(ValueError):
            region = Region(**args)

    def test_invalid_sizes(self):
        args = {
            'name' : "New York 3",
            'slug' : "nyc3",
            'features': [
                "private_networking",
                "backups",
                "ipv6",
                "metadata",
                "install_agent",
                "storage",
                "image_transfer"   
            ],
            'available': True,
            'sizes': [
                "s-1vcpu-1gb",
                "s-1vcpu-2gb",
                True,
            ]
        }

        with self.assertRaises(ValueError):
            region = Region(**args)

if __name__ == '__main__':
    unittest.main(buffer=False)
