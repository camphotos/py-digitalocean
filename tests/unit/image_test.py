#!/usr/local/bin/python3

import unittest
from unittest.mock import patch

from digitalocean.exceptions import *
from digitalocean.image import Image

VALID_BASE_ARGS = {
    'name': "14.04 x64",
    'distribution': "Ubuntu",
    'public': True,
    'regions': [
        "nyc1",
        "ams1",
        "sfo1"
    ]
}

VALID_RETURNED_ARGS = {
    "id": 6918990,
    "name": "14.04 x64",
    "distribution": "Ubuntu",
    "slug": "ubuntu-16-04-x64",
    "public": True,
    "regions": [
        "nyc1",
        "ams1",
        "sfo1",
        "nyc2",
        "ams2",
        "sgp1",
        "lon1",
        "nyc3",
        "ams3",
        "nyc3"
    ],
    "created_at": "2014-10-17T20:24:33Z",
    "min_disk_size": 20,
    "size_gigabytes": 2.34,
    "description": "",
    "tags": [],
}


class TestImage(unittest.TestCase):

    def test_valid_creation(self):

        img = Image(**VALID_BASE_ARGS)
        self.assertTrue(isinstance(img, Image))
        self.assertEqual(img.name(), "14.04 x64")
        self.assertEqual(img.distribution(), "Ubuntu")
        self.assertEqual(img.public(), True)
        self.assertEqual(len(img.regions()), 3)
        self.assertEqual(img.regions()[0], 'nyc1')
        self.assertEqual(img.id(), None)
        self.assertEqual(img.type(), None)
        self.assertEqual(img.slug(), None)
        self.assertEqual(img.created_at(), None)
        self.assertEqual(img.min_disk_size(), None)
        self.assertEqual(img.size_gigabytes(), None)
        self.assertEqual(img.description(), None)
        self.assertEqual(len(img.tags()), 0)
        self.assertEqual(img.status(), None)

    def test_valid_return(self):

        img = Image(**VALID_RETURNED_ARGS)
        self.assertTrue(isinstance(img, Image))
        self.assertEqual(img.id(), 6918990)
        self.assertEqual(img.name(), "14.04 x64")
        self.assertEqual(img.distribution(), "Ubuntu")
        self.assertEqual(img.slug(), "ubuntu-16-04-x64")
        self.assertEqual(img.public(), True)
        self.assertEqual(len(img.regions()), 10)
        self.assertEqual(img.regions()[0], 'nyc1')
        self.assertEqual(img.created_at(), "2014-10-17T20:24:33Z")
        self.assertEqual(img.min_disk_size(), 20)
        self.assertEqual(img.size_gigabytes(), 2.34)
        self.assertEqual(img.description(), "")
        self.assertEqual(len(img.tags()), 0)

 #XXX consolidate    
    def test_invalid_name(self):
        args = VALID_RETURNED_ARGS.copy()

        #
        # Missing Name
        del(args['name'])
        with self.assertRaises(ValueError):
            img = Image(**args)
        
        #
        # Name is a string
        args['name'] = 123
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_distribution(self):
        args = VALID_RETURNED_ARGS.copy()

        #
        # Missing distribution
        del(args['distribution'])
        with self.assertRaises(ValueError):
            img = Image(**args)

        #
        # Invalid distribution
        args['distribution'] = "invalid"
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_public(self):
        args = VALID_RETURNED_ARGS.copy()
        
        #
        # Missing Public
        del(args['public'])
        with self.assertRaises(ValueError):
            img = Image(**args)

        #
        # Invalid public
        args['public'] = "blah"
        with self.assertRaises(ValueError):
            img = Image(**args)

    @patch('digitalocean.utils.validate_list_of_strings')
    def test_invalid_regions(self, m_list_of_strings):
        args = VALID_RETURNED_ARGS.copy()

        #
        # Missing Region
        del(args['regions'])
        with self.assertRaises(ValueError):
            img = Image(**args)

        #
        # Invalid list of regions
        m_list_of_strings.side_effect = ValueError
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_id(self):
        args = VALID_RETURNED_ARGS.copy()

        #
        # ID is a float
        args['id'] = 1.23
        with self.assertRaises(ValueError):
            img = Image(**args)

        #
        # ID is a string
        args['id'] = "123"
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_type(self):
        args = VALID_RETURNED_ARGS.copy()
    
        #
        # type not in list
        args['type'] = 'awesome_image'
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_slug(self):
        args = VALID_RETURNED_ARGS.copy()
        args['slug'] = 123
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_created_at(self):
        args = VALID_RETURNED_ARGS.copy()

        args['created_at'] = 123
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_min_disk_size(self):
        args = VALID_RETURNED_ARGS.copy()

        #
        # not int
        args['min_disk_size'] = "abc"
        with self.assertRaises(ValueError):
            img = Image(**args)

        #
        # -1
        args['min_disk_size'] = -1
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_size_gigabyte(self):
        args = VALID_RETURNED_ARGS.copy()
    
        #
        # int
        args['size_gigabytes'] = 1
        with self.assertRaises(ValueError):
            img = Image(**args)

        # String
        args['size_gigabytes'] = "abc"
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_description(self):
        args = VALID_RETURNED_ARGS.copy()
    
        #
        # int
        args['description'] = 1
        with self.assertRaises(ValueError):
            img = Image(**args)

    @patch('digitalocean.utils.validate_list_of_strings')
    def test_invalid_tags(self, m_list_of_strings):
        args = VALID_RETURNED_ARGS.copy()
        args['tags'] = [1]

        #
        # Invalid list of strings
        m_list_of_strings.side_effect = ValueError
        with self.assertRaises(ValueError):
            img = Image(**args)


    def test_invalid_status(self):
        args = VALID_RETURNED_ARGS.copy()

        args['status'] = 'rockingNRolling'
        with self.assertRaises(ValueError):
            img = Image(**args)

if __name__ == '__main__':
    unittest.main(buffer=False)
