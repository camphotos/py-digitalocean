#!/usr/local/bin/python3

import unittest

from digitalocean.exceptions import *

class TestExceptions(unittest.TestCase):

    def test_NotAuthorized(self):
        e = NotAuthorized()
        self.assertTrue(isinstance(e, NotAuthorized))

    def test_NotFound(self):
        e = NotFound()
        self.assertTrue(isinstance(e, NotFound))

    def test_InternalError(self):
        e = InternalError()
        self.assertTrue(isinstance(e, InternalError))

    def test_InvalidRequest(self):
        e = InvalidRequest("blah")
        self.assertTrue(isinstance(e, InvalidRequest))

        with self.assertRaises(TypeError):
            e = InvalidRequest()

if __name__ == '__main__':
    unittest.main(buffer=False)
