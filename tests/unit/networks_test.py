#!/usr/local/bin/python3

import unittest
from unittest.mock import patch

from digitalocean.exceptions import *
from digitalocean.networks import Networks
from digitalocean.networks.v4 import V4
from digitalocean.networks.v6 import V6

BASE_ARGS = {
    "v4": [
        {
            "ip_address": "10.128.192.124",
            "netmask": "255.255.0.0",
            "gateway": None,
            "type": "private"
        },

    ],
    "v6": [
        {
            "ip_address": "2604:a880:0:1010::18a:a001",
            "netmask": 64,
            "gateway": "2604:a880:0:1010::1",
            "type": "public"
        }
    ]
}

class TestNetworks(unittest.TestCase):
    
    @patch('digitalocean.networks.v4.V4.__init__')
    @patch('digitalocean.networks.v6.V6.__init__')
    def test_valid_creation(self, m_v4, m_v6):

        m_v4.return_value = None
        m_v6.return_value = None

        data = BASE_ARGS.copy()
        networks = Networks(**data)
        self.assertTrue(isinstance(networks, Networks))
        self.assertEqual(len(networks.v4()), 1)
        self.assertEqual(len(networks.v6()), 1)

    @patch('digitalocean.networks.v4.V4.__init__')
    def test_invalid_v4(self, m_v4):
        data = BASE_ARGS.copy()

        m_v4.side_effect = ValueError

        with self.assertRaises(ValueError):
            networks = Networks(**data)

    @patch('digitalocean.networks.v6.V6.__init__')
    def test_invalid_v6(self, m_v6):
        data = BASE_ARGS.copy()

        m_v6.side_effect = ValueError

        with self.assertRaises(ValueError):
            networks = Networks(**data)

if __name__ == '__main__':
    unittest.main(buffer=False)
