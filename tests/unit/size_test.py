#!/usr/local/bin/python3

import unittest

from digitalocean.exceptions import *
from digitalocean.size import Size

class TestSize(unittest.TestCase):

    def test_valid_creation(self):

        args = {
            'slug': "s-1vcpu-1gb",
            'memory': 1024,
            'vcpus': 1,
            'disk': 25,
            'transfer': 1.0,
            'price_monthly': 5,
            'price_hourly': 0.00743999984115362,
            'regions': [
                "ams2",
                "ams3"
            ],
            'available': True,
            'description': "Basic"
        }

        size = Size(**args)
        self.assertTrue(isinstance(size, Size))
        self.assertEqual(size.slug(), 's-1vcpu-1gb')
        self.assertEqual(size.memory(), 1024)
        self.assertEqual(size.vcpus(), 1)
        self.assertEqual(size.disk(), 25)
        self.assertEqual(size.transfer(), 1.0)
        self.assertEqual(size.price_monthly(), 5)
        self.assertEqual(size.price_hourly(), 0.00743999984115362)
        self.assertEqual(len(size.regions()), 2)
        self.assertEqual(size.regions()[0], 'ams2')
        self.assertTrue(size.available())
        self.assertEqual(size.description(), 'Basic')



if __name__ == '__main__':
    unittest.main(buffer=False)
