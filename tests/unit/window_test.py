#!/usr/local/bin/python3

import unittest

from digitalocean import Window

class TestWindow(unittest.TestCase):

    def test_valid_creation(self):

        args = {
            'start': '1',
            'end': '2'
        }

        wind = Window(**args)
        self.assertTrue(isinstance(wind, Window))
        self.assertEqual(wind.start(), '1')
        self.assertEqual(wind.end(), '2')



if __name__ == '__main__':
    unittest.main(buffer=False)
