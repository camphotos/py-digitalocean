#!/usr/local/bin/python3

import unittest

from digitalocean.exceptions import *
from digitalocean.networks.v4 import V4

class Testv4(unittest.TestCase):


    def test_valid_creation(self):
        args = {
            'ip_address': '1.1.1.2',
            'netmask': '255.255.255.0',
            'type': 'public',
            'gateway': '1.1.1.1'
        }

        v4 = V4(**args)
        self.assertTrue(isinstance(v4, V4))

        self.assertEqual(v4.ip_address(), '1.1.1.2')
        self.assertEqual(v4.netmask(), '255.255.255.0')
        self.assertEqual(v4.type(), 'public')
        self.assertEqual(v4.gateway(), '1.1.1.1')


if __name__ == '__main__':
    unittest.main(buffer=False)
