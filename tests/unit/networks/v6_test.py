#!/usr/local/bin/python3

import unittest

from digitalocean.exceptions import *
from digitalocean.networks.v6 import V6

class Testv6(unittest.TestCase):


    def test_valid_creation(self):
        args = {
            "ip_address": "2604:a880:0:1010::18a:a001",
            "netmask": 64,
            "gateway": "2604:a880:0:1010::1",
            "type": "public"
        }

        v6 = V6(**args)
        self.assertTrue(isinstance(v6, V6))

        self.assertEqual(v6.ip_address(), "2604:a880:0:1010::18a:a001")
        self.assertEqual(v6.netmask(), 64)
        self.assertEqual(v6.type(), 'public')
        self.assertEqual(v6.gateway(), '2604:a880:0:1010::1')


if __name__ == '__main__':
    unittest.main(buffer=False)
