#!/usr/local/bin/python3

import unittest
from unittest.mock import patch

from digitalocean.exceptions import *
from digitalocean import Droplet
from digitalocean import Window
from digitalocean import Image
from digitalocean import Size
from digitalocean.networks import Networks
from digitalocean import Region

BASE_VALID_ARGS = {
    "id": 3164444,
    "name": "example.com",
    "memory": 1024,
    "vcpus": 1,
    "disk": 25,
    "locked": False,
    "status": "active",
    "created_at": "2020-07-21T18:37:44Z",
    "features": [
        "backups",
        "private_networking",
        "ipv6"
    ],
    "backup_ids": [
        53893572
    ],
    "next_backup_window": {
        "start": "2020-07-30T00:00:00Z",
        "end": "2020-07-30T23:00:00Z"
    },
    "snapshot_ids": [
        67512819
    ],
    "image": {
        "id": 63663980,
        "name": "20.04 (LTS) x64",
        "distribution": "Ubuntu",
        "slug": "ubuntu-20-04-x64",
        "public": True,
        "regions": [
            "ams2",
            "ams3",
            "blr1",
            "fra1",
            "lon1",
            "nyc1",
            "nyc2",
            "nyc3",
            "sfo1",
            "sfo2",
            "sfo3",
            "sgp1",
            "tor1"
        ],
        "created_at": "2020-05-15T05:47:50Z",
        "type": "snapshot",
        "min_disk_size": 20,
        "size_gigabytes": 2.36,
        "description": "",
        "tags": [ ],
        "status": "available",
        "error_message": ""
    },
    "volume_ids": [ ],
    "size": {
        "slug": "s-1vcpu-1gb",
        "memory": 1024,
        "vcpus": 1,
        "disk": 25,
        "transfer": 1.0,
        "price_monthly": 5,
        "price_hourly": 0.00743999984115362,
        "regions": [],
        "available": True,
        "description": "Basic"
    },
    "size_slug": "s-1vcpu-1gb",
    "networks": {
        "v4": [
            {
                "ip_address": "10.128.192.124",
                "netmask": "255.255.0.0",
                "gateway": None,
                "type": "private"
            },

        ],
        "v6": [
            {
                "ip_address": "2604:a880:0:1010::18a:a001",
                "netmask": 64,
                "gateway": "2604:a880:0:1010::1",
                "type": "public"
            }
        ]
    },
    "region": {
        "name": "New York 3",
        "slug": "nyc3",
        "features": [
            "private_networking",
            "backups",
            "ipv6",
            "metadata",
            "install_agent",
            "storage",
            "image_transfer"
        ],
        "available": True,
        "sizes": []
    },
    "tags": [
        "web",
        "env:prod"
    ],
    "vpc_uuid": "760e09ef-dc84-11e8-981e-3cfdfeaae000"
}

class TestDroplet(unittest.TestCase):

    def test_valid_creation(self):

        drop = Droplet(**BASE_VALID_ARGS)
        self.assertTrue(isinstance(drop, Droplet))
        self.assertEqual(drop.id(), 3164444)
        self.assertEqual(drop.name(), 'example.com')
        self.assertEqual(drop.memory(), 1024)
        self.assertEqual(drop.disk(), 25)
        self.assertFalse(drop.locked())
        self.assertEqual(drop.status(), 'active')
        self.assertEqual(drop.created_at(), "2020-07-21T18:37:44Z")
        self.assertEqual(len(drop.features()), 3)
        self.assertEqual(len(drop.backup_ids()), 1)
        self.assertTrue(isinstance(drop.next_backup_window(), Window))
        self.assertEqual(len(drop.snapshot_ids()), 1)
        self.assertTrue(isinstance(drop.image(), Image))
        self.assertEqual(len(drop.volume_ids()), 0)
        self.assertTrue(isinstance(drop.size(), Size))
        self.assertEqual(drop.size_slug(), "s-1vcpu-1gb")
        self.assertTrue(isinstance(drop.networks(), Networks))
        self.assertTrue(isinstance(drop.region(), Region))
        self.assertEqual(len(drop.tags()), 2)
        self.assertEqual(drop.vpc_uuid(), "760e09ef-dc84-11e8-981e-3cfdfeaae000")

        #
        # Missing Optionals
        args = BASE_VALID_ARGS.copy()
        del(args['vpc_uuid'])

        drop = Droplet(**args)
        self.assertTrue(isinstance(drop, Droplet))
        drop = Droplet(**args)
        self.assertTrue(isinstance(drop, Droplet))
        self.assertEqual(drop.id(), 3164444)
        self.assertEqual(drop.name(), 'example.com')
        self.assertEqual(drop.memory(), 1024)
        self.assertEqual(drop.disk(), 25)
        self.assertFalse(drop.locked())
        self.assertEqual(drop.status(), 'active')
        self.assertEqual(drop.created_at(), "2020-07-21T18:37:44Z")
        self.assertEqual(len(drop.features()), 3)
        self.assertEqual(len(drop.backup_ids()), 1)
        self.assertTrue(isinstance(drop.next_backup_window(), Window))
        self.assertEqual(len(drop.snapshot_ids()), 1)
        self.assertTrue(isinstance(drop.image(), Image))
        self.assertEqual(len(drop.volume_ids()), 0)
        self.assertTrue(isinstance(drop.size(), Size))
        self.assertEqual(drop.size_slug(), "s-1vcpu-1gb")
        self.assertTrue(isinstance(drop.networks(), Networks))
        self.assertTrue(isinstance(drop.region(), Region))
        self.assertEqual(len(drop.tags()), 2)
        self.assertEqual(drop.vpc_uuid(), None)

    def test_invalid_id(self):
        args = BASE_VALID_ARGS.copy()

        #
        # Mising ID
        del(args['id'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # ID is a float
        args['id'] = 1.23
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # ID is a string
        args['id'] = "123"
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

    def test_invalid_name(self):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing Name
        del(args['name'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # Name is an int
        args['name'] = 123
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

    def test_invalid_memory(self):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing Memory
        del(args['memory'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # Memory is a float
        args['memory'] = 1.23
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # Memory is a string
        args['memory'] = "abc"
        with self.assertRaises(ValueError):
            drop = Droplet(**args)


    def test_invalid_vcpus(self):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing vcpus
        del(args['vcpus'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # vcpus is an a float
        args['vcpus'] = 12.3
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # vcpus is a string
        args['vcpus'] = "123"
        with self.assertRaises(ValueError):
            drop = Droplet(**args)


    def test_invalid_disk(self):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing Disk
        del(args['disk'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # Disk is a float
        args['disk'] = 1.23
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # Disk is a string
        args['disk'] = "123"
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

    
    def test_invalid_locked(self):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing locked
        del(args['locked'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # Non Boolean
        args['locked'] = 1
        with self.assertRaises(ValueError):
            drop = Droplet(**args)


    def test_invalid_status(self):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing Status
        del(args['status'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # Status is a string but not in list
        args['status'] = 'RockingNRollin'
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # Non String       
        args['status'] = 1
        with self.assertRaises(ValueError):
            drop = Droplet(**args)
    

    def test_invalid_created_at(self):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing Created at
        del(args['created_at'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # Non string
        args['created_at'] = 123
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # Non valid date
        args['created_at'] = "1/2/13"
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

    @patch('digitalocean.utils.validate_list_of_strings')
    def test_invalid_features(self, d_validate_list_of_strings):
        args = BASE_VALID_ARGS.copy()
        
        #
        # Missing features
        del(args['features'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # non list of strings
        d_validate_list_of_strings.side_effect = ValueError
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

    @patch('digitalocean.utils.validate_list_of_ints')
    def test_invalid_backup_ids(self, d_validate_list_of_ints):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing backup ids
        del(args['backup_ids'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # non list of ints
        d_validate_list_of_ints.side_effect = ValueError
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

    @patch('digitalocean.window.Window.__init__')
    def test_invalid_next_backup_window(self, d_window):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing next backup window
        del(args['next_backup_window'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # Invalid backup window
        d_window.side_effect = ValueError
        with self.assertRaises(ValueError):
            drop = Droplet(**args)
   
    def test_invalid_snapshot_ids(self):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing snapshot ids
        del(args['snapshot_ids'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # invalid snaphot ids
        # Mock

    @patch('digitalocean.image.Image.__init__')
    def test_invalid_image(self, d_image):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing Image
        del(args['image'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # Invalid Image
        d_image.side_effect = ValueError

    @patch('digitalocean.utils.validate_list_of_ints')
    def test_invalid_volume_ids(self, d_validate_list_of_ints):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing volume Ids
        del(args['volume_ids'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # Invalid volume ids
        d_validate_list_of_ints.side_effect = ValueError
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

    @patch('digitalocean.size.Size.__init__')
    def test_invalid_size(self, d_size):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing size
        del(args['size'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)
        
        #
        # Invalid size
        d_size.side_effect = ValueError
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

    def test_invalid_size_slug(self):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing size slug
        del(args['size_slug'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # size slug is an int
        args['size_slug'] = 1
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

    @patch('digitalocean.networks.Networks.__init__')
    def test_invalid_networks(self, d_networks):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing networks
        del(args['networks'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # invalid networks
        d_networks.side_effect = ValueError
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

    @patch('digitalocean.region.Region.__init__')
    def test_invalid_region(self, d_region):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing region
        del(args['region'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # invalid region
        d_region.side_effect = ValueError
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

    @patch('digitalocean.utils.validate_list_of_strings')
    def test_invalid_tags(self, d_validate_list_of_strings):
        args = BASE_VALID_ARGS.copy()

        #
        # Missing tags
        del(args['tags'])
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

        #
        # invalid list of tags
        d_validate_list_of_strings.side_effect = ValueError
        with self.assertRaises(ValueError):
            drop = Droplet(**args)

    def test_invalid_vpc_uuid(self):
        args = BASE_VALID_ARGS.copy()

        #
        # vpc_uuid is an int
        args['vpc_uuid'] = 123
        with self.assertRaises(ValueError):
            drop = Droplet(**args)


    def test_list_droplets(self):
        args = BASE_VALID_ARGS.copy()
        #XXX
        pass

    def test_create_droplet(self):
        args = BASE_VALID_ARGS.copy()
        #XXX
        pass

    def test_get_droplet(self):
        args = BASE_VALID_ARGS.copy()
        #XXX
        pass

    def test_delete(self):
        args = BASE_VALID_ARGS.copy()
        #XXX
        pass

if __name__ == '__main__':
    unittest.main(buffer=False)
