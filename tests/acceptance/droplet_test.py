#!/usr/local/bin/python3

import unittest

from digitalocean.exceptions import *
from digitalocean import Droplet

class TestDroplet(unittest.TestCase):

    ID = None
    DROPLET = None

    def test_1_create_droplet(self):
        data = {
            "name": "example.com",
            "region": "nyc3",
            "size": "s-1vcpu-1gb",
            "image": "ubuntu-20-04-x64",
            "backups": True,
            "ipv6": True,
            "monitoring": True,
            "tags": [
                "env:prod",
                "web"
            ],
            "user_data": "#cloud-config\nruncmd:\n - touch /test.txt\n",    
        }

        drop = Droplet.create_droplet(data)
        self.assertTrue(isinstance(drop, Droplet))

        global ID
        ID = drop.id()

    def test_2_list_droplets(self):
        droplets = Droplet.list_droplets()
        self.assertTrue(droplets)
        self.assertTrue(isinstance(droplets[0], Droplet))

    def test_3_get_droplet(self):
        droplet = Droplet.get_droplet(ID)
        self.assertTrue(isinstance(droplet, Droplet))
        self.assertEqual(droplet.id(), ID)
 
        global DROPLET
        DROPLET = droplet

    def test_4_del_droplet(self):
        ret = DROPLET.delete()
        self.assertTrue(ret)        

if __name__ == '__main__':
    unittest.main(buffer=False)
