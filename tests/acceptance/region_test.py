#!/usr/local/bin/python3

import unittest

from digitalocean.exceptions import *
from digitalocean import Region

class TestRegion(unittest.TestCase):

    def test_list_regions(self):
        regions = Region.list_regions()
        self.assertTrue(regions)
        self.assertTrue(isinstance(regions[0], Region))

if __name__ == '__main__':
    unittest.main(buffer=False)
