#!/usr/local/bin/python3

import unittest

from digitalocean.exceptions import *
from digitalocean import Size

class TestSize(unittest.TestCase):

    def test_list_sizess(self):
        sizes = Size.list_sizes()
        self.assertTrue(sizes)
        self.assertTrue(isinstance(sizes[0], Size))

if __name__ == '__main__':
    unittest.main(buffer=False)
