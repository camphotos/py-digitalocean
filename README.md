# py-digitalocean

[![pipeline status](https://gitlab.com/camphotos/py-digitalocean/badges/main/pipeline.svg)](https://gitlab.com/camphotos/py-digitalocean/-/commits/main)[![coverage report](https://gitlab.com/camphotos/py-digitalocean/badges/main/coverage.svg)](https://gitlab.com/camphotos/py-digitalocean/-/commits/main)


## Overview

## Building

`$ make build`

## Unit Testing

`$ make unit-test`

## Acceptance Testing

`$ make acceptance-test`

## TODO
version bump
USAGE.md file
CONTRIBUTING.md file
changelog
